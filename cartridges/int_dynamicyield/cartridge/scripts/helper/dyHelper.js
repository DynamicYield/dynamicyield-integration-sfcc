// This file for both - Controllers and SFRA version, contains main functions to handle all possible trackEvents by dynamicYield object.

var BasketMgr = require('dw/order/BasketMgr');
var OrderMgr = require('dw/order/OrderMgr');
var ProductMgr = require('dw/catalog/ProductMgr');
var CatalogMgr = require('dw/catalog/CatalogMgr');
var DYlib = require('*/cartridge/scripts/lib/DYlib');

/**
 * 
 * @param {Object} uniParameterMap parameter map
 * @param {boolean} isSFRA is SFRA or not
 * @returns {Object} recommendation context
 */
function getRecommendationContext( uniParameterMap, isSFRA) {
	
    var page_type = "";
    var pid = "";
    var cgid = "";

    //if its SFRA version
    if(isSFRA == true) {
        page_type = uniParameterMap.page_type;
        pid = uniParameterMap.pid;
        cgid = uniParameterMap.cgid;
    }
    //if its controllers version ( xxx.value ) necessary 
    else {
        page_type = uniParameterMap.page_type.value;
        pid = uniParameterMap.pid.value;
        cgid = uniParameterMap.cgid.value;
    }

    if( empty(page_type) ) {
        DYlib.log('info', 'parameter - page_type - is empty and must not be empty');
        return;
    }
	
    var recommendationContext = {};
	
    recommendationContext.type = page_type;
    recommendationContext.data = [];
	
    switch( page_type ){

        case "HOMEPAGE":
            break;
        case "CATEGORY":
            // load category id paramter passed by isml via action call with parameter
            if( empty(cgid) ) {
                DYlib.log('info', 'parameter - cgid - is empty and must not be empty');
                return;
            }
            var category = CatalogMgr.getCategory( cgid );
			
            while(!empty(category)){
                if (!category.root){
                    recommendationContext.data.unshift(category.getDisplayName());
                }
                category = category.getParent();
            }
            break;			
        case "PRODUCT":
            if( empty(pid) ) {
                DYlib.log('info', 'parameter - pid - is empty and must not be empty');
                return;
            }
            var product = ProductMgr.getProduct( pid ),
                validProductID;
			
            if(product.productSet) {
                validProductID = DYlib.getValidProductID(product.productSetProducts[0]);
            } else {
                validProductID = DYlib.getValidProductID(product);
            }
			
            recommendationContext.data = [validProductID];
            break;
        case "CART":
            var productLineItems = !empty( BasketMgr.getCurrentBasket() ) ? BasketMgr.getCurrentBasket().getProductLineItems() : [];
			
            for (var i in productLineItems) {
                var productID = DYlib.getValidProductID(ProductMgr.getProduct(productLineItems[i].productID));
                recommendationContext.data.push(productID);
	        }
            break;
        default:
            recommendationContext.type = 'OTHER';
    }
    return recommendationContext;
}

/**
 * 
 * @param {Object} params query params
 * @param {string} eventName event name
 * @returns {Object} prepared request
 */
function getDYresponse( params, eventName ) {
	
    var DYresponse = {
        doCall: true,
        eventName: '',
        properties: {}
    };
    var	productLineItems;
    var currentBasket;
    var i;

    switch(eventName){
        case "Add to Cart":
            var product = ProductMgr.getProduct(params.productId);
			
            params.productId = DYlib.getValidProductID(product);
			
            currentBasket = BasketMgr.getCurrentBasket();
            productLineItems = currentBasket.getProductLineItems();
			
            DYresponse.eventName = eventName;
            DYresponse.properties.dyType = 'add-to-cart-v1';
            DYresponse.properties.uniqueRequestId = DYlib.getUniqueId();
            DYresponse.properties.cart = [];
            for (i in productLineItems) {
                var lineProduct = ProductMgr.getProduct(productLineItems[i].productID),
                    productID = DYlib.getValidProductID(lineProduct),
                    lineItem = {
                        productId: productID,
                        quantity: productLineItems[i].quantity.value,
                        itemPrice: productLineItems[i].basePrice.value
                    }
                DYresponse.properties.cart.push(lineItem);
	        }
	
            DYresponse.properties.productId = params.productId;
			
            DYresponse.properties.quantity = params.quantity ? Number(params.quantity) : 1;
			
            DYresponse.properties.currency = currentBasket.getCurrencyCode();
            DYresponse.properties.value = currentBasket.getProductLineItems(product.ID)[0].basePrice.value * DYresponse.properties.quantity;
            session.custom.DYaddedItem = '';
            break;
			
        case "setAddedItem":
            if( !empty(params.productId) && !empty(params.quantity) ){
                params.productId = DYlib.getValidProductID(ProductMgr.getProduct(params.productId));
				
                var addedItem = {
                    productId: params.productId,
                    quantity: params.quantity
                }
                session.custom.DYaddedItem = JSON.stringify(addedItem);
            }
            DYresponse.doCall = false;
            break;
		
        case "Purchase":
            DYresponse.eventName = eventName;
            DYresponse.properties.dyType = 'purchase-v1';
            DYresponse.properties.uniqueRequestId = DYlib.getUniqueId();
            DYresponse.properties.uniqueTransactionId = DYlib.getUniqueId();
            DYresponse.properties.cart = [];
            var order = OrderMgr.getOrder(params.orderId);
            productLineItems = order.getProductLineItems();
            for (i in productLineItems) {
                var lineProduct = ProductMgr.getProduct(productLineItems[i].productID),
                    productID = DYlib.getValidProductID(lineProduct),
				
                    lineItem = {
                        productId: productID,
                        quantity: productLineItems[i].quantity.value,
                        itemPrice: productLineItems[i].basePrice.value
                    }
                DYresponse.properties.cart.push(lineItem);
	        }
            DYresponse.properties.value = order.getAdjustedMerchandizeTotalPrice().value;
            DYresponse.properties.currency = order.getCurrencyCode();
            break;
			
        case "Account":
            if(!empty(session.customer.profile)){
                var now = new Date();
                var registrationTime = session.customer.profile.creationDate;
                var lastLoginTime = session.customer.profile.lastLoginTime;
                if( (now - registrationTime) < 10000 ){
                    DYresponse.eventName = 'Signup';
                    DYresponse.properties.dyType = 'signup-v1';
                } else if( (now - lastLoginTime) < 10000 ){
                    DYresponse.eventName = 'Login';
                    DYresponse.properties.dyType = 'login-v1';
                } else {
                    DYresponse.doCall = false;
                }
	
                DYresponse.properties.uniqueRequestId = DYlib.getUniqueId();
                DYresponse.properties.hashedEmail = DYlib.hashText(session.customer.profile.email);
            } else {
                DYresponse.doCall = false;
            }
            break;
		
        case "Newsletter Subscription":
            if(!empty(params.email)){
                DYresponse.eventName = eventName;
                DYresponse.properties.dyType = 'newsletter-subscription-v1';
                DYresponse.properties.uniqueRequestId = DYlib.getUniqueId();
                DYresponse.properties.hashedEmail = DYlib.hashText(params.email);
            }
            break;
			
        case "setRemovedItem":
            if( !empty(params.productId) && !empty(params.quantity) ){
                var removedItem = {
                    productId: params.productId,
                    quantity: params.quantity
                }
                session.custom.DYremovedItem = JSON.stringify(removedItem);
            }
            DYresponse.doCall = false;
            break;
			
        case "Remove from Cart":
            if(!empty(session.custom.DYremovedItem)){
                var removedItem = JSON.parse(session.custom.DYremovedItem),
                    product = ProductMgr.getProduct(removedItem.productId);
				
                currentBasket = BasketMgr.getCurrentBasket();
                productLineItems = !empty(currentBasket) ? currentBasket.getProductLineItems() : [];
                DYresponse.eventName = eventName;
                DYresponse.properties.dyType = 'remove-from-cart-v1';
                DYresponse.properties.uniqueRequestId = DYlib.getUniqueId();
                DYresponse.properties.currency = currentBasket.getCurrencyCode();
                DYresponse.properties.value = Number(product.getPriceModel().price.value) * Number(removedItem.quantity);
                DYresponse.properties.productId = DYlib.getValidProductID(ProductMgr.getProduct(removedItem.productId));
                DYresponse.properties.quantity = removedItem.quantity;
                DYresponse.properties.cart = [];
                for (i in productLineItems) {
                    var lineProduct = ProductMgr.getProduct(productLineItems[i].productID),
                        productID = DYlib.getValidProductID(lineProduct);
						
                    lineItem = {
                        productId: productID,
                        quantity: productLineItems[i].quantity.value,
                        itemPrice: productLineItems[i].basePrice.value
                    }
					
                    DYresponse.properties.cart.push(lineItem);
	            }
                session.custom.DYremovedItem='';
            } else {
                DYresponse.doCall = false;
            }
	
            break;
	
        case "Add to Wishlist":
            if(!empty(params.productId)){
                DYresponse.eventName = eventName;
                DYresponse.properties.dyType = 'add-to-wishlist-v1';
                DYresponse.properties.uniqueRequestId = DYlib.getUniqueId();
                DYresponse.properties.productId = DYlib.getValidProductID(ProductMgr.getProduct(params.productId));;
            }
            break;
	
        case "Sort Items":
            if( !empty(params.sortBy) && !empty(params.sortOrder) ){
                DYresponse.eventName = eventName;
                DYresponse.properties.dyType = 'sort-items-v1';
                DYresponse.properties.uniqueRequestId = DYlib.getUniqueId();
                DYresponse.properties.sortBy = params.sortBy.toUpperCase();
                DYresponse.properties.sortOrder = params.sortOrder.toUpperCase();
            }
            break;
			
        case "Filter Items":
            if( !empty(params.type) && !empty(params.value) ){
                DYresponse.eventName = eventName;
                DYresponse.properties.dyType = 'filter-items-v1';
                DYresponse.properties.uniqueRequestId = DYlib.getUniqueId();
                DYresponse.properties.filterType = params.type;
                DYresponse.properties.filterStringValue = params.value;
            }
            break;
	
        case "Change Attribute":
            if( !empty(params.type) && !empty(params.value) ){
                DYresponse.eventName = eventName;
                DYresponse.properties.dyType = 'change-attr-v1';
                DYresponse.properties.uniqueRequestId = DYlib.getUniqueId();
                DYresponse.properties.attributeType = params.type;
                DYresponse.properties.attributeValue = params.value;
            }
            break;
	
        case "Promo Code Entered":
            if(!empty(params.code)){
                DYresponse.eventName = eventName;
                DYresponse.properties.dyType = 'enter-promo-code-v1';
                DYresponse.properties.uniqueRequestId = DYlib.getUniqueId();
                DYresponse.properties.code = params.code;
                if(!empty(params.ajax) && params.ajax){
                    if(!empty(session.custom.DYcoupons)){
                        session.custom.DYcoupons=session.custom.DYcoupons+'|';
                    }
                    session.custom.DYcoupons=session.custom.DYcoupons+params.code;
                }
            }
            break;
	
        case "Keyword Search":
            if(!empty(params.keywords)){
                DYresponse.eventName = eventName;
                DYresponse.properties.dyType = 'keyword-search-v1';
                DYresponse.properties.uniqueRequestId = DYlib.getUniqueId();
                DYresponse.properties.keywords = params.keywords;
            }
            break;
	
        case "Sync cart":
            if(session.custom.DYnewSession=='true'){
                currentBasket = BasketMgr.getCurrentBasket();
                DYresponse.eventName = eventName;
                DYresponse.properties.dyType = 'sync-cart-v1';
                productLineItems = !empty(currentBasket) ? currentBasket.getProductLineItems() : [];
                DYresponse.properties.cart = [];
                for (i in productLineItems) {
                    var lineProduct = ProductMgr.getProduct(productLineItems[i].productID),
                        productID = DYlib.getValidProductID(lineProduct),
				
                        lineItem = {
                            productId: productID,
                            quantity: productLineItems[i].quantity.value,
                            itemPrice: productLineItems[i].basePrice.value
                        }
                    DYresponse.properties.cart.push(lineItem);
	            }
                session.custom.DYnewSession = 'false';
            } else {
                DYresponse.doCall = false;
            }
            break;
			
        default:;

    } // swtich

    return DYresponse;
}

module.exports = {
    getRecommendationContext: getRecommendationContext,
    getDYresponse: getDYresponse
};
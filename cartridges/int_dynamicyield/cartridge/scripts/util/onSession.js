'use strict';

var Status = require('dw/system/Status');

exports.onSession = function () {
    session.custom.DYnewSession = 'true';
    return new Status(Status.OK);
};

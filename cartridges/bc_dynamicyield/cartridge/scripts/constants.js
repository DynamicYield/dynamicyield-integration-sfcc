let File = require('dw/io/File');
let Site = require('dw/system/Site');

/**
@description productfeed.csv if required file name, please don't change it.
*/
const EXPORT_FILENAME = 'productfeed.csv';
const EXPORT_FILE_SEPARATOR = ',';
const EXPORT_FILE_QUOTE = '"';
const EXPORT_PATH = 'DynamicYield';
const BASE_CDN = 'cdn.dynamicyield.com';
const EURO_CDN = 'cdn-eu.dynamicyield.com';

module.exports.EXPORT_FILENAME = EXPORT_FILENAME;
module.exports.EXPORT_FILE_SEPARATOR = EXPORT_FILE_SEPARATOR;
module.exports.EXPORT_FILE_QUOTE = EXPORT_FILE_QUOTE;
module.exports.EXPORT_PATH = EXPORT_PATH;
module.exports.BASE_CDN = BASE_CDN;
module.exports.EURO_CDN = EURO_CDN;

module.exports.PRODUCT_IMAGE_VIEWTYPE = 'medium';

module.exports.EXPORT_FILEPATH = File.IMPEX + File.SEPARATOR + EXPORT_PATH + File.SEPARATOR + Site.current.ID + File.SEPARATOR + EXPORT_FILENAME;
module.exports.EXPORT_FOLDERPATH = File.IMPEX + File.SEPARATOR + EXPORT_PATH + File.SEPARATOR + Site.current.ID + File.SEPARATOR;
module.exports.EXPORT_FIELDS = [
    'name',
    'url',
    'sku',
    'group_id',
    'price',
    'in_stock',
    'categories',
    'image_url',
    'keywords'
];

module.exports.AWS_DEFAULT_REGION_EU = 'eu-central-1';
module.exports.AWS_DEFAULT_REGION_US = 'us-east-1';
module.exports.AWS_DEFAULT_SERVICE = 's3'; 

let ProductMgr = require('dw/catalog/ProductMgr');
let PriceBookMgr = require('dw/catalog/PriceBookMgr');
let Logger = require('dw/system/Logger').getLogger('DynamicYield');
let Bytes = require('dw/util/Bytes');
let S3TransferClient = require('../service/S3TransferClient');
let SecureRandom = require('dw/crypto/SecureRandom');
let Constants = require('../constants');

/**
 * Main initilaization
 */
function API() {};

API.prototype = {
    // cache price books
    priceBooks: [],
    /**
     * @returns {string} is DY cartridge is enabled
     */
    isDynamicYieldEnabled: function() {
    	return dw.system.Site.current.preferences.custom['DY_isEnabled'];
    },
    /**
     * @returns {string} is catalog export feature is enabled
     */
    isDynamicYieldExportEnabled: function() {
    	return dw.system.Site.current.preferences.custom['DY_isCatalogExportEnabled'];
    },
    /**
     * @returns {string} DY access key
     */
    getAccessKey: function() {
    	return dw.system.Site.current.preferences.custom['DY_AccessKeyID'];
    },
    /**
     * @returns {string} DY secret access key
     */
    getSecretKey: function() {
        return dw.system.Site.current.preferences.custom['DY_SecretAccessKey'];
    },
    /**
     * @returns {number} DY section ID
     */
    getSectionID: function() {
    	return dw.system.Site.current.preferences.custom['DY_SectionID'];
    },
    /**
     * @returns {string} endpoint URL
     */
    getEndpointURL: function() {
        return dw.system.Site.current.preferences.custom['DY_EndpointURL'];
    },
    /**
     * @returns {string} function for generating unique ID
     */
    getUniqueId: function(){
    	let random = new SecureRandom;
    	return random.nextInt(1000000000).toString();
    },
    /**
     * @returns {string} allowed locales
     */
    getAllowedLocales: function() {
    	return dw.system.Site.current.allowedLocales;
    },
    
    /**
     * @returns {boolean} is DY european CDN enabled
     */
    isEuroEnable: function() {
    	return dw.system.Site.current.preferences.custom['DY_euroToggle'];
    },
    
    /**
     * @returns {boolean} enable CDN integration
     */
    isCDNIntegration: function() {
    	return dw.system.Site.current.preferences.custom['DY_Enable_CDN_integration'];
    },
    
    /**
     * @returns {string} CDN custom url
     */
    getCustomCDN: function() {
    	return dw.system.Site.current.preferences.custom['DY_custom_CDN_URL'];
    },
    
    /**
     * 
     * @returns {string} returns enabled cdn
     */
    getCDN: function(){
    	var cdn = Constants.BASE_CDN;
    	if ((this.isCDNIntegration() && this.isEuroEnable()) || (!this.isCDNIntegration() && !this.isEuroEnable())){
    		return cdn;
    	} else {
    		cdn = this.isCDNIntegration() ? this.getCustomCDN() : Constants.EURO_CDN;
    		return cdn;
    	}
    },
    
    /**
     * @returns {boolean} is async attribute is added to loading scripts
     */
    isAsyncEnable: function(){
    	return dw.system.Site.current.preferences.custom['DY_enable_async'];
    },
    /**
     * @param {string} str input string
     * @returns {string} hashed text
     */
    hashText: function(str){
    	let Crypto = require('dw/crypto'),
    		MessageDigest = Crypto.MessageDigest(Crypto.MessageDigest.DIGEST_SHA_256);
    	
    	return Crypto.Encoding.toHex(MessageDigest.digestBytes(new Bytes(str))); 
    },
    
    /**
    *  @return {Array} with custom product attributes
    */
    getProductAttributes: function() {
        return dw.system.Site.current.preferences.custom['DY_ProductAttributes'];
    },
    
    /**
    * @return {Iterator} all site products
    */
    getProductIterator: function() {
        return ProductMgr.queryAllSiteProducts();
    },
    /**
     * @returns {Object} assigned pricebooks
     */
    getPriceBooks: function() {
    	return this.priceBooks.length
    		? this.priceBooks
    		: this.priceBooks = PriceBookMgr.getSitePriceBooks();		
    },
    /**
     * @description Get required fields from constants, and custom attrs from SitePrefs, check if Custom pref exist
     * in the Product object and add them to required fields
     * @returns {Object} list of exported fields
     */
    getExportFieldsList: function() {
        let products = this.getProductIterator(),
            requiredAttributes = Constants.EXPORT_FIELDS,
            customAttributes = this.getProductAttributes(),
            priceBooks = this.getPriceBooks(),
            allowedLocales = this.getAllowedLocales(),
            localledFields = [],
            priceAttrs = [],
            productAttrs,
            existingAttrs = [];

        let productSystemObject = dw.object.SystemObjectMgr.describe('Product');
        
        if(customAttributes) {
        	existingAttrs = customAttributes.split(',').filter(function(attr) {
                return !!productSystemObject.getCustomAttributeDefinition(attr.trim())
            });
        }
         
        if(existingAttrs.length) {
        	requiredAttributes = requiredAttributes.concat(existingAttrs);
        }
        
        if(allowedLocales.size() > 1) {
        	for(let i in allowedLocales) {
        		let loc = allowedLocales[i];
        		if(loc == 'default') continue;
        		
        		for(let k in Constants.EXPORT_FIELDS) {
        			let field = Constants.EXPORT_FIELDS[k];
            		if(field == 'price') continue;
            		
            		localledFields.push('lng:'+ loc + ':' + field);
            	}
        	}
        	requiredAttributes = requiredAttributes.concat(localledFields);
        }
        
        for (let i in priceBooks) {
        	let priceBook = priceBooks[i];
        	priceAttrs.push(priceBook.getCurrencyCode());
        }
        
        //remove dublicates
        priceAttrs = priceAttrs.filter(function(currency, position, self) {
        	return self.indexOf(currency) == position;
        });
        
        products.close();
        
        return {
        	'required': requiredAttributes,
        	'currencies': priceAttrs
        }
    },
    
    /**
     * @description retrieve product price from price books using by given currency code
     * @param {Object} product SFCC product
     * @param {string} fieldName currency field
     * @return {string} price of product
     */
    getCurrencyValue: function(product, fieldName) {
    	let priceBooks = this.getPriceBooks();
    	
    	if((product.optionProduct || product.master || product.bundle) && product.getPriceModel().price.value != 0) {
   		 	//doit
	   	 } else {
	   		 if(product.variant) {
	   			//doit
	   		 } else {
	   			try{
	   				product = product.variants[0];
	   			} catch (e) {
	   				// if it is simple product it doesnot contains variants 
	   				product = product;
	   			}	   			
	   		 }
	   	 }
    	
    	 for (let i in priceBooks) {
         	let priceBook = priceBooks[i];
         	
         	if(priceBook.currencyCode == fieldName) {
         		let price = product.priceModel.getPriceBookPrice(priceBook.ID).value;
         		// even if price isn't available in the selected price book, price will be 0
         		return price;
         	}
        }
    },
    
    /**
     * @param {Object} product SFCC product
     * @returns {boolean} returns true if product is simple product
     */
    isSimpleProduct: function (product){
    	return !product.optionProduct && !product.master && !product.bundle && !product.variant && (product.variants.length == 0);
    },
    
    /**
     * @description format currency fields
     * @param  {Array} fieldList list of fields
     * @return {Array} formatted fileds
     */
    formatFieldList: function(fieldList) {
    	return fieldList.required.concat(fieldList.currencies.map(function(currency) {
    			return currency+':price'; 
    		}
    	));
    },
    /**
     * @param {Object} product  dw.catalog.Product
     * @param {string} fieldName locale field
     * @returns {string} extracted value from product
     */
    getProductValue: function(product, fieldName) {
    	let localedField = fieldName.split(':');
    	let localeName = 'default';
    	
    	if(!empty(localedField[1])) {
    		localeName = localedField[1];
    		fieldName = localedField[2];
    	}
    	
    	request.setLocale(localeName);
    
        switch (fieldName) {
            case 'name':
                return product.name;
                break;
            case 'url':
                return dw.web.URLUtils.http('Product-Show', 'pid', product.ID, 'lang', localeName);
                break;
            case 'sku':
            	 return product.ID;
            	 break;
            case 'group_id':
                return product.variant ? product.masterProduct.ID : product.ID;
            	 break;
            case 'price':
            	if((product.optionProduct || product.master || product.bundle) && product.getPriceModel().price.value != 0) {
            		 return product.getPriceModel().price.value;
            	 } else {
            		 if(product.variant) {
            			 return product.getPriceModel().price.value;
            		 } else {
            			 if (product.variants.size() == 0){
           				 if(this.isSimpleProduct(product)){
            					 return product.getPriceModel().price.value;
           				 }
            				 /* If master product hasn't variants and price then we cannot determine price in default SG logic */
            				 return '';
            			 }
            			 else{
            				 return product.variants[0].getPriceModel().price.value;
            			 }
            		 }
            	 }
            	 break;
            case 'in_stock':
            	 return product.availabilityModel.isInStock();
            	 break;
            case 'categories':
            	 var isEmptyCategories = false;
        	 
            	 if(empty(product.categories)) {
            		 isEmptyCategories = true;
            	 }
            	 //we using primary category for feed generation
            	 return this.categoryListing(product, isEmptyCategories);
            	 
            	 break;
            case 'image_url':
            	 let image = product.getImage(Constants.PRODUCT_IMAGE_VIEWTYPE);
            	 return !empty(image) ? image.absURL : '';
            	 break;
            case 'keywords':
            	 let productCategories = product.categories,
        	 	 feedCategories = [];
        	 
            	 if(empty(productCategories)) {
            		 productCategories = product.masterProduct.categories;
            	 }

            	 if(productCategories.length) {
            		 for(let i in productCategories) {
            			 let cat = productCategories[i], root = false;
        			 
            			 while(!cat.root) {
            				 if (cat.online){
            					 feedCategories.push(cat.displayName);
            				 }
            				 cat = cat.parent;
            				 root = cat.root
            			 }
        			 
            		 }
            		 return feedCategories.join('|');
            	 } else {
            		 return '';
            	 }
            	 /* You can use next line if keywords needs to be extracted from native SFCC "keywords" field */
            	 //return !empty(product.pageKeywords) ? product.pageKeywords.split(', ').join('|') : '';
            	 break;
            // custom product attributes
            default:
                return this.getCustomFieldValue(product.custom[fieldName.trim()] || '', fieldName.trim());
        }
    },
    /**
     * @param {string} fieldValue custom field to extract
     * @return {string} extracted field
     */
    getCustomFieldValue: function(fieldValue) {
    	switch(fieldValue.constructor.name) {
    		case 'dw.content.MarkupText':
    			return fieldValue.source;
    			break;
    		case 'dw.value.EnumValue':
    			return fieldValue.displayValue;
    			break;
    		case 'Array':
    			return fieldValue.join('|');
    			break;
    		default:
    			return fieldValue || '';
    	}
    },
    /**
     * @description If the PDP has variants with different price (at least one variation)
     * 				we should put to recomendationContext id one of those variations (randomly) because
     * 				impossible to know what variation will be selected by user
     * @param {dw.catalog.Product} product SFCC product
     * @return {string} product ID
     */
    getValidContextProductID: function(product) {
    	if(product.variants.size()) {
    		return product.ID != this.getValidProductID(product.variants[0])
    			? product.variants[0].ID
    		    : product.ID;
    	} else {
    		return product.ID;
    	}
    	
    },
    /**
     * @description This function needed for determining if the product has variation with the different prices
     * 				In case if has: we should use VariationID for the "Add to cart", "Remove from Cart", "Add to wish list"
     * 				API calls
     * 				If not: master product id
     * @param {dw.catalog.Product} product SFCC product
     * @return {number} productID
     */
    getValidProductID: function(product) {
    	let addedPrice,
    	useMasterID = true,
    	masterProduct,
    	variants;
    	try {
    		masterProduct = product.master || product.optionProduct || product.bundle ? product : product.masterProduct;
        } catch (e) {
            masterProduct = product;
        }
    	let productID = masterProduct.ID
    	    	
    	if(masterProduct.bundle || (masterProduct.bundled && !masterProduct.variant)) {
    		return masterProduct.ID;
    	}
    	
    	if(!empty(masterProduct.variants)) {
    		variants = masterProduct.variants;
    		
    		addedPrice = masterProduct.variants[0].priceModel.price.value;
    		
    		for(let i in variants) {
    			let variant = variants[i];
    	
    			if(variant.priceModel.price.value != addedPrice) {
    				useMasterID = false;
    				break;
    			}
    		}
    	}
    	
    	if(useMasterID) {
    		return productID;
    	} else if (product.variant) {
    		return product.ID;
    	} else if(!empty(masterProduct.variants)) {
    		return masterProduct.variants[0].ID;
    	} else {
    		return productID;
    	}
    },
    
    /**
     * @description If product has variations with the different prices, return all variations for this product
     * @param {dw.catalog.Product} masterProduct SFCC master product
     * @return {Collection|boolean} list of valid products or false
     */
    getValidProductList: function(masterProduct) {
    	let price = 0;
    	
    	if(!empty(masterProduct.variants)) {
    		let variants = masterProduct.variants;
    		
    		price = variants[0].priceModel.price.value;
    		
    		for(let i in variants) {
    			let variant = variants[i];
    			
    			if(variant.priceModel.price.value != price) {
    				return masterProduct.variants;
    			}
    		}
    	}
    	
    	return false;
    },
    
    /**
     * @param {File} file csv product feed
     * @return {boolean|Error} true or throwing error
     */
    sendExportFile: function(file) {
    	let self = this;
    	let service = dw.svc.LocalServiceRegistry.createService("dynamicyield.S3.upload", {
            execute: function (HTTPCService, params) {
                let S3Client = new S3TransferClient(
                        self.getEndpointURL().trim(), /* S3 bucket ID */
                        self.getAccessKey().trim(), /* AWS Access Key ID */
                        self.getSecretKey().trim(), /* AWS Secret Access Key */
                        self.isEuroEnable() ? Constants.AWS_DEFAULT_REGION_EU : Constants.AWS_DEFAULT_REGION_US , /* AWS Zone ID */  
                        'text/csv', /* standard MIME types */
                        900000, /* Timeout in milliseconds, recommended to use maximum value - 15 min */
                        self.getSectionID().trim() /* Folder within bucket */
                    ), response;
					
                response = S3Client.putBinary(Constants.EXPORT_FILENAME, file);

                if (response.statusCode != 200) {
                    throw new Error(response.errorText);
                } else {
                    return true;
                }
            },
            parseResponse: function(response) {
                return response;
            }
        })
			
        return service.call();
    },
    
    /**
     * @param {string} level level of the log
     * @param {string} message logging message
     * @param {Object} data data to store
     */
    log: function(level, message, data) {
        Logger[level](message, data);
    },

    /**
     * @param {Object} product SFCC product
     * @param {boolean} isMasterProduct is master product or not
     * @returns {string} list of categories separated by comma
     */
    categoryListing: function(product, isMasterProduct){
    	var feedCategories = [];
    	if (isMasterProduct){
    		product = product.masterProduct;
    	}
    	if (product.getPrimaryCategory()){
   		 let cat = product.primaryCategory, root = false;
			 
			 while(!cat.root) {
				 feedCategories.unshift(cat.displayName);
				 cat = cat.parent;
				 root = cat.root
			 }
			 return feedCategories.join('|');
   	 }
   	 // if there is no primary category then we using classification category 
   	 if (product.getClassificationCategory()){
   		 let cat = product.classificationCategory, root = false;
			 
			 while(!cat.root) {
				 feedCategories.unshift(cat.displayName);
				 cat = cat.parent;
				 root = cat.root
			 }
			 return feedCategories.join('|');
   	 }
   	 // if there is no primary and classification category then we will use first category in collection
   	 if (!product.categories.isEmpty()){
   		 let cat = product.categories.toArray()[0], root = false;
			 
			 while(!cat.root) {
				 feedCategories.unshift(cat.displayName);
				 cat = cat.parent;
				 root = cat.root
			 }
			 return feedCategories.join('|');
   	 }
    }
};

module.exports = new API();

'use strict';

/** @type {dw.util.Bytes} */
let Bytes = require('dw/util/Bytes');
/** @type {dw.util.Calendar} */
let Calendar = require('dw/util/Calendar');
/** @type {dw.crypto.Encoding} */
let Encoding = require('dw/crypto/Encoding');
/** @type {dw.io.File} */
let File = require('dw/io/File');
/** @type {dw.net.HTTPClient} */
let HTTPClient = require('dw/net/HTTPClient');
/** @type {dw.system.Logger} */
let logger = require('dw/system/Logger').getLogger('DynamicYield');
/** @type {dw.crypto.Mac} */
let Mac = require('dw/crypto/Mac');
/** @type {dw.crypto.MessageDigest} */
let MessageDigest = require('dw/crypto/MessageDigest');
/** @type {dw.io.RandomAccessFileReader} */
let RandomAccessFileReader = require('dw/io/RandomAccessFileReader');
/** @type {dw.util.StringUtils} */
let StringUtils = require('dw/util/StringUtils');
/** @type {S3FileInfo} */
let S3FileInfo = require('./S3FileInfo');
/** @type {Function} */
let parseUri = require('../lib/URLlib.js').parseUri;

// S3 Transfer Client Settings
/**
 * @todo fix poor usage of not quite constants and also constants for things that should be instance variables.
 */
let ACCESSKEY,
    ALGORITHM = "AWS4-HMAC-SHA256",
    AWSREGION,
    AWSSERVICE = "s3",
    BUCKETNAME,
    CANONICALQUERYSTRING	= encodeURI(''),
    CONTENTTYPE,
    CREDENTIALSCOPE = "",
    DELIMITER = "/",
    DATESTAMP = StringUtils.formatCalendar(new Calendar(), "yyyyMMdd"),
    DATETIMESTAMP = StringUtils.formatCalendar(new Calendar(), "yyyyMMdd'T'HHmmss'Z'"),
    ENDPOINT	 = "https://{{bucket}}.s3.amazonaws.com/",
    HOST	 = "{{bucket}}.s3.amazonaws.com",
    PREFIX = '',
    REMOTEFOLDER, /* This let doesn't even seem to be used! */
    SECRETACCESSKEY,
    SIGNEDHEADERS = "host;x-amz-content-sha256;x-amz-date",
    SIGNINGKEY = "",
    STRINGTOSIGN	 = "",
    TIMEOUT;

/**
 * S3TransferClient provides an interface for reading, writing and deleting files from Amazon's Simple Storage Service (S3)
 *
 * @class
 *
 * @param {string} bucketName bucket name
 * @param {string} accessKey access key
 * @param {string} secretAccessKey secret access key
 * @param {string} region region
 * @param {string} contentType content type
 * @param {number} timeout timeout
 * @param {string} remoteFolder remote folder
 *
 * @todo fix use of private 'constants' as setting variables
 */
function S3TransferClient(bucketName, accessKey, secretAccessKey, region, contentType, timeout, remoteFolder) {
    ACCESSKEY			= accessKey;
    AWSREGION			= region;
    BUCKETNAME			= bucketName;
    CONTENTTYPE			= contentType;
    REMOTEFOLDER		= remoteFolder;
    SECRETACCESSKEY		= secretAccessKey;
    TIMEOUT				= timeout;

    // Initialize the transfer client
    init(bucketName, secretAccessKey);
    this.urlObject = parseUri(ENDPOINT + REMOTEFOLDER);
}

/**
 * Initializes the settings for the S3 transfer client
 *
 * @param {string} bucketName bucket name
 * @param {string} secretAccessKey secret access key
 * @todo fix use of private 'constants' as setting variables
 */
function init(bucketName, secretAccessKey) {
    CREDENTIALSCOPE	= getCredentialScope(DATESTAMP, AWSREGION, AWSSERVICE);
    ENDPOINT	 = ENDPOINT.replace('{{bucket}}', bucketName);
    HOST	 = HOST.replace('{{bucket}}', bucketName);
    SIGNINGKEY = getSignatureKey(secretAccessKey, DATESTAMP, AWSREGION, AWSSERVICE);
};

/**
 * Retrieves the passed file/directory
 *
 * @param {string} fullFileName full file name
 * @param {File} localFile (dw.io.File)
 * @returns {boolean} downloaded or not
 */
S3TransferClient.prototype.getBinary = function (fullFileName, localFile) {
    let response = execute('GET', fullFileName, null, localFile);

    if (response.statusCode === 200) {
        return true;
    } else {
        logger.error("S3TransferClient.ds: Unable to download [{0}]. An error occurred with status code [{1}] and error text [{2}]", fullFileName, response.statusCode, response.errorText);
        return false;
    }
};

/**
 * Delivers the passed file/directory
 *
 * @param {string} fullFileName full file name
 * @param {File} localFile local file
 * @returns {Object} status of result
 */
S3TransferClient.prototype.putBinary = function (fullFileName, localFile) {
    return execute('PUT', fullFileName, localFile, null);
};

/**
 * Deletes the remote file
 *
 * @param {string} fullFileName full file name
 * @returns {boolean} succsess or not
 */
S3TransferClient.prototype.del = function (fullFileName) {
    let response = execute('DELETE', fullFileName, null, null);

    if (response.statusCode === 200) {
        return true;
    } else {
        logger.error("S3TransferClient.ds: Unable to delete [{0}]. An error occurred with status code [{1}] and error text [{2}]", fullFileName, response.statusCode, response.errorText);
        return false;
    }
};

/**
 * Perform the AWS call and return the HTTPClient used.
 *
 * @param {string} httpMethod HTTP method
 * @param {string} fullPath full path
 * @param {File} outGoingFile outgoing file
 * @param {File} incomingFile incoming file
 * @returns {HTTPClient} (dw.net.HTTPClient)
 *
 * @todo fix use of private 'constants' as setting variables
 */
function execute(httpMethod, fullPath, outGoingFile, incomingFile) {
    /** @type {String} */
    let	authorizationHeader	= '',
        /** @type {String} */
        canonicalHeaders = '',
        /** @type {String} */
        canonicalRequest = '',
        /** @type {String} */
        canonicalURI = '',
        /** @type {HTTPClient} */
        httpClient = new HTTPClient(),
        /** @type {String} */
        payloadHash = '',
        /** @type {String} */
        signature = '',
        /** @type {String} */
        stringToSign = '',
        /** @type {String} */
        methodEndpoint = '',
        /** @type {Mac} */
        hmac = new Mac(Mac.HMAC_SHA_256);

    // Create payload hash
    if (outGoingFile !== null) {
        // Sending files requires a hash of the file to be submitted.
        payloadHash = getPayloadHash(outGoingFile);
    } else {
        // A standard GET request, with no payload, submits a hashed empty string
        payloadHash = getPayloadHash('');
    }

    // Create the canonical headers
    canonicalHeaders = getCanonicalHeaders(httpMethod, HOST, payloadHash, DATETIMESTAMP);

    // Set signed headers
    SIGNEDHEADERS = getSignedHeaders(httpMethod);

    // If we're requesting a path, the prefix needs to be set
    if (fullPath.lastIndexOf('.') !== fullPath.length - 4 && fullPath.lastIndexOf('.') !== fullPath.length - 3) {
        PREFIX = fullPath;
        canonicalURI = '/';
    } else {
        PREFIX = '';
        canonicalURI = "/" + REMOTEFOLDER + "/" + fullPath;
    }

    // Set the canonicalQueryString
    setCanonicalQueryString(httpMethod, PREFIX, DELIMITER);

    // Create the canonical request
    canonicalRequest = getCanonicalRequest(httpMethod, canonicalURI, CANONICALQUERYSTRING, canonicalHeaders, SIGNEDHEADERS, payloadHash);

    // Get the string to sign
    stringToSign = getStringToSign(ALGORITHM, DATETIMESTAMP, CREDENTIALSCOPE, canonicalRequest);

    // Generate Signature
    signature = Encoding.toHex(hmac.digest(stringToSign, SIGNINGKEY));

    // Create authorization header
    authorizationHeader = getAuthorizationHeader(ALGORITHM, ACCESSKEY, CREDENTIALSCOPE, SIGNEDHEADERS, signature);

    // Get the correct endpoint for the method/path/querystring
    methodEndpoint = createEndpoint(httpMethod, fullPath, CANONICALQUERYSTRING);

    // Setup connection
    httpClient.open(httpMethod, methodEndpoint);
    httpClient.setTimeout(TIMEOUT);
    httpClient.setRequestHeader('content-type', CONTENTTYPE);
    httpClient.setRequestHeader('x-amz-content-sha256', payloadHash);
    httpClient.setRequestHeader('x-amz-date', DATETIMESTAMP);
    httpClient.setRequestHeader('Authorization', authorizationHeader);

    // Sending files requires the content-length header to be set
    if (outGoingFile !== null) {
        httpClient.setRequestHeader('Content-Length', outGoingFile.length());
    }

    // Make the request
    if (httpMethod === 'PUT') {
        httpClient.send(outGoingFile);
    } else {
        if (empty(incomingFile)) {
            // GET/DELETE requests (e.g. directory listing/delete a file)
            httpClient.send();
        } else {
            // GET request & receive file (e.g. request a file)
            httpClient.sendAndReceiveToFile(null, incomingFile);
        }
    }

    return httpClient;
}

/**
 * Creates the AWS signing key
 *
 * @param {string} key key 
 * @param {string} dateStamp timestamp
 * @param {string} regionName region
 * @param {string} serviceName service
 * @returns {string} The Signing Key to use for authorization.
 */
function getSignatureKey(key, dateStamp, regionName, serviceName) {
    /** @type {Bytes} */
    let signedDate,
        /** @type {Bytes} */
        signedRegion,
        /** @type {Bytes} */
        signedService,
        /** @type {Bytes} */
        signingKey,
        /** @type {String} */
        awsKey = "AWS4" + key;

    signedDate = sign(awsKey, dateStamp);
    signedRegion = sign(signedDate, regionName);
    signedService = sign(signedRegion, serviceName);
    signingKey = sign(signedService, 'aws4_request');

    return signingKey;
};

/**
 * Signs the passed message using the passed key. key can be either {String} or {Bytes}
 *
 * @param {(string|Bytes)} key key
 * @param {string} msg string
 * @returns {string} The SHA256 Key for the given key & message.
 */
function sign(key, msg) {
    let hmac = new Mac(Mac.HMAC_SHA_256);

    return hmac.digest(msg, key);
};

/**
 * Returns the hashed form of the payload
 *
 * @param {(string|File)} payload params
 * @returns {string} Hex-encoded hash
 */
function getPayloadHash(payload) {
    /** @type {MessageDigest} */
    let messageDigest = new MessageDigest(MessageDigest.DIGEST_SHA_256),
        /** @type {Bytes} */
        payloadHash,
        /** @type {RandomAccessFileReader} */
        fileReader,
        /** @type {Bytes} */
        currentByte;

    // GET requests will pass an instance of string, PUT requests will send File
    if (typeof payload === "string") {
        payloadHash = messageDigest.digestBytes(new Bytes(payload));

        return Encoding.toHex(payloadHash);
    } else if (payload instanceof File) {
        fileReader = new RandomAccessFileReader(payload);

        while ((currentByte = fileReader.readBytes(1)) !== null) {
            messageDigest.updateBytes(currentByte);
        };

        return Encoding.toHex(messageDigest.digest());
    }
};

/**
 * Returns the aws formatted canonical headers
 *
 * @param {string} httpMethod HTTP method
 * @param {string} host host
 * @param {string} payloadHash params
 * @param {string} date date
 * @returns {string} Multi-line String of Canonical headers
 *
 * @todo fix use of private 'constants' as setting variables
 */
function getCanonicalHeaders(httpMethod, host, payloadHash, date) {
    /** @type {String} */
    let canonicalHeaders =	'host:' + host + '\n' +
									'x-amz-content-sha256:' + payloadHash + '\n' +
									'x-amz-date:' + date + '\n';
    if (httpMethod === 'GET') {
        canonicalHeaders = 	'content-type:' + CONTENTTYPE + '\n' + canonicalHeaders;
    }

    return canonicalHeaders;
};

/**
 * Returns the aws formatted signed headers
 *
 * @param {string} httpMethod HTTP method
 * @returns {string} Appropriate headers
 */
function getSignedHeaders(httpMethod) {
    /** @type {String} */
    let signedHeaders = "host;x-amz-content-sha256;x-amz-date";

    if (httpMethod === 'GET') {
        signedHeaders = 'content-type;' + signedHeaders;
    }

    return signedHeaders;
}

/**
 * Returns the aws formatted canonical request
 *
 * @param {string} httpMethod HTTP method
 * @param {string} canonicalURI canonical URI
 * @param {string} canonicalQueryString canonical query string
 * @param {string} canonicalHeaders canonical headers
 * @param {string} signedHeaders signed headers
 * @param {string} payloadHash params
 * @returns {string} Multi-line HTTP request headers
 */
function getCanonicalRequest(httpMethod, canonicalURI, canonicalQueryString, canonicalHeaders, signedHeaders, payloadHash) {
    /** @type {String} */
    let canonicalRequest = httpMethod + '\n' +
									canonicalURI + '\n' +
									canonicalQueryString + '\n' +
									canonicalHeaders + '\n' +
									signedHeaders + '\n' +
									payloadHash;

    return canonicalRequest;
};

/**
 * Returns the aws formatted credential scope
 *
 * @param {string} dateStamp timestamp
 * @param {string} awsRegion region
 * @param {string} awsService service
 * @returns {string} request string
 */
function getCredentialScope(dateStamp, awsRegion, awsService) {
    return	dateStamp + '/' +
			awsRegion + '/' +
			awsService + '/' +
			'aws4_request';
};

/**
 * Create the string to sign
 *
 * @param {string} algorithm sign algorithm
 * @param {string} dateTimeStamp timestamp
 * @param {string} credentialScope credentials
 * @param {string} canonicalRequest canonical request
 * @returns {string} string prepared for sign
 */
function getStringToSign(algorithm, dateTimeStamp, credentialScope, canonicalRequest) {
    /** @type {MessageDigest} */
    let messageDigest = new MessageDigest(MessageDigest.DIGEST_SHA_256);

    return	algorithm + '\n' +
			dateTimeStamp + '\n' +
			credentialScope + '\n' +
			Encoding.toHex(messageDigest.digestBytes(new Bytes(canonicalRequest)));
};

/**
 * Create the authorization header
 *
 * @param {string} algorithm sign algorithm
 * @param {string} accessKeyId timestamp
 * @param {string} credentialScope credentials
 * @param {string} signedHeaders signed headers
 * @param {string} signature signature
 * @returns {string} authorization header
 */
function getAuthorizationHeader(algorithm, accessKeyId, credentialScope, signedHeaders, signature) {
    /** @type {String} */
    let authorizationHeader	=	algorithm + ' ' +
											'Credential=' + accessKeyId + '/' + credentialScope + ',' +
											'SignedHeaders=' + signedHeaders + ',' +
											'Signature=' + signature;

    return authorizationHeader;
};

/**
 * Encode slashes
 *
 * @param {string} input string
 * @returns {string} string without slashes
 */
function encodeSlash(input) {
    return input.replace(/\//g, '%2F');
}

/**
 * Parses the AWS key (file key)
 *
 * @param {string} key file key
 * @param {string} delimiter delimiter
 * @returns {boolean} is key directory or not
 */
function isKeyADirectory(key, delimiter) {
    /** @type {Number} */
    let lastOccurence = key.lastIndexOf(delimiter);

    // Delimiter at the end === a directory
    if (lastOccurence === (key.length -1)) {
        return true;
    } else {
        return false;
    }
};

/**
 * Creates the endpoint connection string
 *
 * @param {string} httpMethod HTTP method
 * @param {string} fullPath full path
 * @param {string} canonicalQueryString canonical query string
 * @returns {string} endpoint connection string
 *
 * @todo fix use of private 'constants' as setting variables
 */
function createEndpoint(httpMethod, fullPath, canonicalQueryString) {
    /** @type {String} */
    let methodEndpoint = '';

    // Generate the correct endpoint for the passed method/path/file
    switch (httpMethod) {
        case 'GET':
            if (fullPath.lastIndexOf('.') !== fullPath.length - 4 && fullPath.lastIndexOf('.') !== fullPath.length - 3) {
                methodEndpoint = ENDPOINT + '?' + canonicalQueryString;
            } else {
                methodEndpoint = ENDPOINT + fullPath;
            }
            break;
        case 'PUT':
            methodEndpoint = ENDPOINT + REMOTEFOLDER + '/' + fullPath;
            methodEndpoint = (canonicalQueryString !== '') ? methodEndpoint + '?' + canonicalQueryString : methodEndpoint;
            break;
        case 'DELETE':
            methodEndpoint = ENDPOINT + fullPath;
            break;
        default:
            throw new Error("Unsupported HTTP Method");
            break;
    }

    return methodEndpoint;
}

/**
 * Sets up the canonical query string
 *
 * @param {string} httpMethod HTTP method
 * @param {string} prefix prefix
 * @param {string} delimiter delimiter
 * @returns {string} canonical query string
 *
 * @todo fix use of private 'constants' as setting variables
 */
function setCanonicalQueryString(httpMethod, prefix, delimiter) {
    switch (httpMethod) {
        case 'GET':
            if (prefix !== '' && delimiter !== '') {
                CANONICALQUERYSTRING = 'delimiter=' + delimiter + '&prefix=' + prefix;
                // Slashes need to be encoded for the signature to match Amazon's
                CANONICALQUERYSTRING = (delimiter === '/') ? encodeSlash(CANONICALQUERYSTRING) : CANONICALQUERYSTRING;
            } else {
                CANONICALQUERYSTRING = '';
            }
            break;
        case 'PUT':
            CANONICALQUERYSTRING = '';
            break;
        case 'DELETE':
            CANONICALQUERYSTRING = '';
            break;
    }

    return CANONICALQUERYSTRING;
}

/** @module int_s3/S3TransferClient */
module.exports = S3TransferClient;

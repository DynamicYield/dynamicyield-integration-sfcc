let File = require('dw/io/File');
let FileWriter = require('dw/io/FileWriter');
let CSVStreamWriter = require('dw/io/CSVStreamWriter');
let Constants = require('../constants');

module.exports.Start = function() {
    let exportFile,
        products,
        product,
        writer,
        fieldList,
        productAttrs,
        Lib = require('../lib/DYlib');

    if(!Lib.isDynamicYieldExportEnabled()) {
        Lib.log('info', "Job/Product Feed isn't enabled", {});
        return true;
    }
  
    new File(Constants.EXPORT_FOLDERPATH).mkdirs();
    exportFile = new File(Constants.EXPORT_FILEPATH);
    
    if(!exportFile.file) exportFile.createNewFile();

    fieldList = Lib.getExportFieldsList();
    writer = new CSVStreamWriter(new FileWriter(exportFile), Constants.EXPORT_FILE_SEPARATOR, Constants.EXPORT_FILE_QUOTE);
    writer.writeNext(Lib.formatFieldList(fieldList));

    products = Lib.getProductIterator();
    
    while(products.hasNext()) {
        product = products.next();
        
        if(!product.optionProduct && !product.master && !product.bundle && !Lib.isSimpleProduct(product)) continue;
        if(product.productSet) continue;
        /**
        * 
        * @param {Object} validProduct exisitng SFCC product
        * @returns {File} file for export
        */
        function insert(validProduct) {
        	//Products with zero price and no variations.
        	if(product.master && product.getPriceModel().price.value == 0 && product.variants.size() == 0) {
        		return false;
        	}
        	
        	productAttrs = [];
        	
        	for(let i in fieldList.required) {
                productAttrs.push(Lib.getProductValue(validProduct, fieldList.required[i]));
            }
            
            for(let i in fieldList.currencies) {
            	productAttrs.push(Lib.getCurrencyValue(validProduct, fieldList.currencies[i]));
            }
            writer.writeNext(productAttrs);
        }
        
        let productList = Lib.getValidProductList(product);
        
        if(productList) {
        	for(let productIndex in productList) {
        		insert(productList[productIndex]);
        	}
        } else {
        	insert(product);
        }
    }
    
    products.close();
    writer.close();

    Lib.sendExportFile(exportFile); 
};

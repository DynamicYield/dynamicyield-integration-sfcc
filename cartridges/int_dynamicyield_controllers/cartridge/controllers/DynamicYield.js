/*
 * Dynamic Yield main controller
 */

let ISML = require('dw/template/ISML'),
    CatalogMgr = require('dw/catalog/CatalogMgr'),
    BasketMgr = require('dw/order/BasketMgr'),
    ProductMgr = require('dw/catalog/ProductMgr'),
    DYlib = require('bc_dynamicyield/cartridge/scripts/lib/DYlib'),
    DynamicYieldHelper = require('*/cartridge/scripts/helper/dyHelper.js'),
    httpParameterMap = request.httpParameterMap;

/**
* render DY template
*/
function render() {

    // Do nothing if - Dynamic Yield is disabled globally - or section ID is not set
    let DY_SectionID = DYlib.getSectionID();
    if (!DYlib.isDynamicYieldEnabled() || empty(DY_SectionID)) { 
        DYlib.log('info', 'Cartridge for Dynamic Yield is disabled');
        return;
    }
	
    // helper section
    let recommendationContext = DynamicYieldHelper.getRecommendationContext( httpParameterMap, false );
	
    // Render template with Dynamic Yield script
    recommendationContext.data = recommendationContext.data.toString();
		
    ISML.renderTemplate('dynamicyield/render', {
        'DY_SectionID' : DY_SectionID,
        'recommendationContext' : recommendationContext,
        'DY_LocaleID': request.locale,
        'DY_cdn' : DYlib.getCDN(),
        'async' : DYlib.isAsyncEnable() ? "async" : ''
    });
}

/**
 * DY API call handler
 * @param {Object} param query params
 */
function getAPIProperties(param){
	
    // Do nothing if - Dynamic Yield is disabled globally - or section ID is not set
    let DY_SectionID = DYlib.getSectionID();
    if (!DYlib.isDynamicYieldEnabled() || empty(DY_SectionID)) { 
        DYlib.log('info', 'Cartridge for Dynamic Yield is disabled');
        return;
    }
	
    let params = httpParameterMap.params!='' ? JSON.parse(httpParameterMap.params) : {};
    let eventName = httpParameterMap.eventName.value;
	
    // helper section
    var DYresponse = DynamicYieldHelper.getDYresponse(params, eventName);
	
    ISML.renderTemplate('util/responsejson', {
        'JSONResponse' : DYresponse
    });
}

exports.Render = render;
exports.Render.public = true;
exports.GetAPIProperties = getAPIProperties;
exports.GetAPIProperties.public = true;
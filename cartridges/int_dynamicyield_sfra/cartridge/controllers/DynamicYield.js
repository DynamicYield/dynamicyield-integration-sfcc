/*
 * Dynamic Yield main controller
 */

'use strict';

var server = require('server');
/* API includes */

var Site = require('dw/system/Site');
var BasketMgr = require('dw/order/BasketMgr');
var ProductMgr = require('dw/catalog/ProductMgr');
var DYlib = require('bc_dynamicyield/cartridge/scripts/lib/DYlib');
var DynamicYieldHelper = require('*/cartridge/scripts/helper/dyHelper.js');


server.get(
    'Render',
    function (req, res, next) {

    	// Do nothing if Dynamic Yield is disabled globally - or section ID is not set
    	var DY_SectionID = DYlib.getSectionID();
    	if (!DYlib.isDynamicYieldEnabled() || empty(DY_SectionID)) { 
    		DYlib.log('info', 'Cartridge for Dynamic Yield is disabled');
    		return;
    	}
    	
    	// Prepare variables
    	var sfraParameterMap = req.querystring;
    	
    	// helper section
    	var recommendationContext = DynamicYieldHelper.getRecommendationContext( sfraParameterMap, true );

    	// Render template with Dynamic Yield script
    	recommendationContext.data = recommendationContext.data.toString();

    	res.render('dynamicyield/render', {
    		'DY_SectionID' : DY_SectionID,
    		'recommendationContext' : recommendationContext,
    		'DY_LocaleID': request.locale,
    		'DY_cdn' : DYlib.getCDN(),
    		'async' : DYlib.isAsyncEnable() ? "async" : ''
    	});

        return next();
    }
);


server.get(
    'GetAPIProperties',
    function (req, res, next) {

    	// Do nothing if Dynamic Yield is disabled globally - or section ID is not set
    	var DY_SectionID = DYlib.getSectionID();
    	if (!DYlib.isDynamicYieldEnabled() || empty(DY_SectionID)) { 
    		DYlib.log('info', 'Cartridge for Dynamic Yield is disabled');
    		return;
    	}
    	
    	var sfraParameterMap = req.querystring;
    	
    	//code
    	var params = sfraParameterMap.params!='' ? JSON.parse(sfraParameterMap.params) : {};
    	var eventName = sfraParameterMap.eventName;

    	// helper section
    	var DYresponse = DynamicYieldHelper.getDYresponse(params, eventName);

    	//render 
    	res.json( DYresponse );

        return next();
    }
);

module.exports = server.exports();
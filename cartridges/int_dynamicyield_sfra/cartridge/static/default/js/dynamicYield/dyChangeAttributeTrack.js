$(document).ready(function() {

    // Options - (input swatcher like : size, width)
    $('div.attribute:not(.quantity)').find('select').on('change', function(el) {
        var $el = $(this).find(':selected');
        dynamicYield.callEvent('Change Attribute', {
            type: $(this).closest('.row').attr("data-attr").trim(),
            value: $el.text().trim()
        });
    });

    // Variations - (buttons swatcher like : color)
    $('div.attribute').find('button.color-attribute').on('click', function(el) {
        dynamicYield.callEvent('Change Attribute', {
            type: $(this).closest('.row').attr('data-attr').trim(),
            value: $(this).find('.swatch-value').attr("data-attr-value").trim()
        });
    });

}); // ready
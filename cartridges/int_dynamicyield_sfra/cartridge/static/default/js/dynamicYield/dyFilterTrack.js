// Refinement tracking
$(document).ready(function() {
	
    // normal first call after page load
    attachRefinementEventHandlers();

    // AGAIN reatach refinment handlers, after clicking, becouse entire section is re-rendered and old listeners lost
    $('.refinements').on('DOMSubtreeModified', function(el) {
        attachRefinementEventHandlers();
    });


    // custom function, that attach eventListeners to all clickable refinement items
    function attachRefinementEventHandlers() {

        // refine items
        $('div.refinement').find('li').on('click', function(el) {

            dynamicYield.callEvent('Filter Items', {
                type: $(this).attr("data-type"),
                value: $(this).attr("data-value")
            });
        });


        //sorting
        $('select[name=sort-order]').off("change");
        $('select[name=sort-order]').on('change', function(el) {
    		
        	var sBy = $("select[name=sort-order] option:selected").attr("data-id");
    		var sOrder = "ASC";
    		if( sBy === "price-high-to-low" || sBy === "product-name-descending") {
    			sOrder = "DESC";
    		}		
    		dynamicYield.callEvent('Sort Items', {
    			sortBy: sBy,
    			sortOrder: sOrder
    		});
        });
    }

}); // ready
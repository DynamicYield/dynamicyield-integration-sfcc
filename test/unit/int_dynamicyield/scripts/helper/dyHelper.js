var assert = require('chai').assert;
var BasketMgr = require('../../../../mocks/dw/order/BasketMgr');
var OrderMgr = require('../../../../mocks/dw/order/OrderMgr');
var ProductMgr = require('../../../../mocks/dw/catalog/ProductMgr');
var CatalogMgr = require('../../../../mocks/dw/catalog/CatalogMgr');
var PriceBookMgr = require('../../../../mocks/dw/catalog/PriceBookMgr');
var Logger = require('../../../../mocks/dw/system/Logger');
var Bytes = require('../../../../mocks/dw/util/Bytes');
var SecureRandom = require('../../../../mocks/dw/crypto/SecureRandom');
var proxyquire = require('proxyquire').noCallThru().noPreserveCache();
var DYlib = proxyquire('../../../../../cartridges/bc_dynamicyield/cartridge/scripts/lib/DYlib', {
    'dw/catalog/ProductMgr': ProductMgr,    
    'dw/catalog/PriceBookMgr': PriceBookMgr,
    'dw/system/Logger': Logger,
    'dw/util/Bytes': Bytes,
    'dw/crypto/SecureRandom': SecureRandom,
    '../service/S3TransferClient': {},
    '../constants': {}
});

global.empty = function(obj) {
    if (obj === null || obj === undefined || obj === '' || (typeof (obj) !== 'function' && obj.length !== undefined && obj.length === 0)) {
        return true;
    } else {
        return false;
    }
};

var dyHelper = proxyquire('../../../../../cartridges/int_dynamicyield/cartridge/scripts/helper/dyHelper', {
    'dw/order/BasketMgr': BasketMgr,
    'dw/order/OrderMgr': OrderMgr,
    'dw/catalog/CatalogMgr': CatalogMgr,
    'dw/catalog/ProductMgr': ProductMgr,
    '*/cartridge/scripts/lib/DYlib': DYlib
});


describe('getRecommendationContext', function () {
    var uniParameterMap = {
        page_type: "test_pagetype",
        pid: "test_productid",
        cgid: "test_categoryid"
    };
    it('getRecommendationContext should return an object of recommendation', function () {
        assert.isObject(dyHelper.getRecommendationContext(uniParameterMap, true));
    });
});

describe('getDYresponse', function () {
    var params = {"keywords":"test keywords"};
    it('getDYresponse should return a prepared request object', function () {
        assert.isObject(dyHelper.getDYresponse(params, "Keyword Search"));
    });
});
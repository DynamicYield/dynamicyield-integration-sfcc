var assert = require('chai').assert;
var request = require('request-promise');
var config = require('../it.config');

describe('DynamicYield-GetAPIProperties', function () {
    this.timeout(25000);

    var myRequest = {
        url: config.baseUrl + '/DynamicYield-GetAPIProperties?eventName%3DNewsletter%20Subscription%26params%3D%7B%22email%22%3A%22test%40test.com%22%7D',
        method: 'GET',
        rejectUnauthorized: false,
        resolveWithFullResponse: true
    };

    it('should return a valid json object', function () {
        return request(myRequest)
            .then(function (response) {
                assert.equal(response.statusCode, 200, 'Expected GetAPIProperties statusCode to be 200.');
            });
    }); 
});

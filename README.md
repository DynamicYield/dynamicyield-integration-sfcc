# Dynamic Yield - Salesforce Commerce Cloud cartridge

The Dynamic Yield cartridge is a self-contained cartridge that can integrate into any project.
The cartridge provides an easy and seamless integration with Dynamic Yield and enables an automatic implementation of Dynamic Yield�s eCommerce solution.

The cartridge will auto install the following components:

* Dynamic Yield tracking script
* Context API 
* eCommerce events
* Product Feed (catalog) export

### Installing

The step by step installation and configuration is described within the user guide. 

### License

Link - https://www.dynamicyield.com/files/HFNDOCS-3696501v1-DY-SFCC-cartridge-EULA.pdf